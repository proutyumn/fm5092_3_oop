﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_3_demo
{
    public abstract class DefinitelyABaseClass
    {
        public string ReturnName()
        {
            return "Great class!";
        }
    }

    public class DefinitelyABaseClassChild : DefinitelyABaseClass
    {

    }
}
