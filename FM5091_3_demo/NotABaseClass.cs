﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_3_demo
{
    public sealed class NotABaseClass
    {
        // this class is marked sealed because it will NEVER be a base class!
        // nothing can inherit from this class now.
    }
}
