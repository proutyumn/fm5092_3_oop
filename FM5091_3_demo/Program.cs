﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_3_demo
{
    class Program
    {
        static void Main(string[] args)
        {
            DefinitelyABaseClassChild d = new DefinitelyABaseClassChild();
            Console.WriteLine(d.ReturnName());

            var g = new { v1 = 5, v2 = 3, v3 = 10 };

            ObjectToInitialize o = new ObjectToInitialize()
            {
                Name = "Doug",
                Age = 50,
                GPA = 1.9
            };

            

        }
    }

    public class ObjectToInitialize
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double GPA { get; set; }

    }

    public class PropertyDemo
    {
        public string Name { get; set; } // very simple and terse property syntax

        string _internal;

        public string External
        {
            get
            {
                return _internal;
            }
            set
            {
                _internal = value;
            }
        }
    }

    public class PropertyChild : PropertyDemo
    {

    }
}
